﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using LogClass;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            cl.LogEventUp += Cl_LogEventUp;
        }

        private void Cl_LogEventUp(object sender, string[] Out)
        {

        }

        ClassLogger cl = new ClassLogger();

        private void one_Click(object sender, RoutedEventArgs e)
        {
            cl.CreateLogger(0, "TestDevice", 0, "");
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            cl.LogMessage(0, 0, 0, new byte[] { 0, 1 });
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            cl.LogMessage(0, 0, 1, new byte[] { 2, 3 });
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            cl.EndWork();
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            string[] Input = new string[2];
            Input[0] = "OK";
            Input[1] = "Critical Emission";
            cl.InputDescriptionErrors(0, Input);
        }
    }
}
