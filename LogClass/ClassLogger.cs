﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogClass
{

    #region ExtraClasses

    class LogInfo
    {
        public string descriptionDevice;
        public int Mode;
        public string TxTFileName;

        public LogInfo(string descriptionDevice, int Mode, string TxTFileName)
        {
            this.descriptionDevice = descriptionDevice;
            this.Mode = Mode;
            this.TxTFileName = TxTFileName;
        }
    }

    class ByteMessage
    {
        public byte Code;
        public byte ErrorCode;
        public byte[] Message;
        public DateTime dateTime;

        public ByteMessage()
        { }

        public ByteMessage(byte Code, byte ErrorCode, byte[] Message, DateTime dateTime)
        {
            this.Code = Code;
            this.ErrorCode = ErrorCode;
            this.Message = Message;
            this.dateTime = dateTime;
        }
    }

    #endregion

    public class ClassLogger
    {
        #region Private Variables

        private string LogName = "LogFile.txt";

        private Dictionary<int, LogInfo> IDLogInfoDictionary = new Dictionary<int, LogInfo>();

        private Dictionary<int, List<ByteMessage>> BigMainDictionary = new Dictionary<int, List<ByteMessage>>();

        private Dictionary<int, Dictionary<int, string>> IDErrorList = new Dictionary<int, Dictionary<int, string>>();

        #endregion

        public delegate void LogEvent(object sender, string[] Out);
        public event LogEvent LogEventUp;

        public void CreateLogger(int ID, string descriptionDevice, int Mode, string TxTFileName)
        {
            if (!IDLogInfoDictionary.ContainsKey(ID))
            {
                IDLogInfoDictionary.Add(ID, new LogInfo(descriptionDevice, Mode, TxTFileName));

                BigMainDictionary.Add(ID, new List<ByteMessage>());
            }
        }

        public void LogMessage(int ID, byte Code, byte ErrorCode, byte[] Message)
        {
            if (IDLogInfoDictionary.ContainsKey(ID))
            {
                BigMainDictionary[ID].Add(new ByteMessage(Code, ErrorCode, Message, DateTime.Now));

                Relaboration();
            }
        }

        public void InputDescriptionErrors(int ID, string[] DescriptionList)
        {
            if (IDLogInfoDictionary.ContainsKey(ID))
            {
                Dictionary<int, string> ErrorList = new Dictionary<int, string>();
                for (int i = 0; i < DescriptionList.Count(); i++)
                {
                    ErrorList.Add(i, DescriptionList[i]);
                }
                IDErrorList.Add(ID, ErrorList);
            }
        }

        public void EndWork()
        {
            for (int i = 0; i < BigMainDictionary.Count; i++)
            {
                ListSorterRemover2(BigMainDictionary.Keys.ElementAt(i), BigMainDictionary[BigMainDictionary.Keys.ElementAt(i)]);
            }
        }

        #region Private Methods

        private void Relaboration()
        {
            for (int i = 0; i < BigMainDictionary.Count; i++)
            {
                ListSorterRemover(BigMainDictionary.Keys.ElementAt(i), BigMainDictionary[BigMainDictionary.Keys.ElementAt(i)]);
            }
        }

        private void ListSorterRemover(int ID, List<ByteMessage> ByteMessageList)
        {
            if (ByteMessageList.Count != 0)
            {
                for (int i = 0; i < ByteMessageList.Count - 1; i++)
                {
                    for (int j = i; j < ByteMessageList.Count - 1; j++)
                    {
                        if (ByteMessageList[i].Code == ByteMessageList[j + 1].Code)
                        {
                            if (ByteMessageList[i].ErrorCode == ByteMessageList[j + 1].ErrorCode)
                            {
                                ByteMessageList.RemoveAt(j + 1);
                                ByteMessageList.RemoveAt(i);
                            }
                            else
                            {
                                Log(ID, ByteMessageList[i], ByteMessageList[j + 1]);
                                ByteMessageList.RemoveAt(j + 1);
                                ByteMessageList.RemoveAt(i);
                            }
                        }
                    }
                }
            }
        }

        private void ListSorterRemover2(int ID, List<ByteMessage> ByteMessageList)
        {
            if (ByteMessageList.Count != 0)
            {
                for (int i = 0; i < ByteMessageList.Count; i++)
                {
                    Log2(ID, ByteMessageList[i]);
                    ByteMessageList.RemoveAt(i);
                }
            }
        }

        private void Log(int ID, ByteMessage byteMessage1, ByteMessage byteMessage2)
        {
            int Mode = IDLogInfoDictionary[ID].Mode;

            string[] Out = GenerateOut(ID, byteMessage1, byteMessage2);

            Switcher(ID, Mode, Out);
        }

        private void Log2(int ID, ByteMessage byteMessage)
        {
            int Mode = IDLogInfoDictionary[ID].Mode;

            string[] Out = GenerateOut2(ID, byteMessage);

            Switcher(ID, Mode, Out);
        }

        private void Switcher(int ID, int Mode, string[] Out)
        {
            switch (Mode)
            {
                case 0:
                case 1:
                    //Запись-добавление в текстовый файл
                    WriteToFile(ID, Mode, Out);
                    break;
                case 2:
                    WriteToConsole(Out);
                    break;
                case 3:
                    GenerateEvent(Out);
                    break;
                default:
                    WriteToFile(ID, Mode, Out);
                    break;
            }
        }

        private string[] GenerateOut(int ID, ByteMessage byteMessage1, ByteMessage byteMessage2)
        {
            string[] Out = new string[7];

            Out[0] = "Device " + ID + " - " + IDLogInfoDictionary[ID].descriptionDevice + ": ";
            Out[1] = "Code " + byteMessage1.Code;
            Out[2] = OutDateTime(byteMessage1.dateTime);
            Out[3] = OutByteMessage(byteMessage1.Message);
            Out[4] = OutDateTime(byteMessage2.dateTime);
            Out[5] = OutByteMessage(byteMessage2.Message);
            Out[6] = "ErrorCode " + byteMessage2.ErrorCode + ExtraInfo(ID, byteMessage2.ErrorCode);
            return Out;
        }

        private string[] GenerateOut2(int ID, ByteMessage byteMessage)
        {
            string[] Out = new string[4];

            Out[0] = "Unanswered requests:";
            Out[1] = "Code " + byteMessage.Code;
            Out[2] = OutDateTime(byteMessage.dateTime);
            Out[3] = OutByteMessage(byteMessage.Message);
            return Out;
        }

        private string OutDateTime(DateTime dateTime)
        {
            string str = dateTime.Date.Day.ToString("00") + "." + dateTime.Date.Month.ToString("00") + "." + dateTime.Date.Year + " " + dateTime.Hour.ToString("00") + ":" + dateTime.Minute.ToString("00") + ":" + dateTime.Second.ToString("00") + ":" + dateTime.Millisecond.ToString("000");
            return str;
        }

        private string OutByteMessage(byte[] byteMessage)
        {
            string str = "";
            for (int i = 0; i < byteMessage.Count(); i++)
            {
                str = str + byteMessage[i].ToString() + " ";
            }
            return str;
        }

        private string ExtraInfo(int ID, byte ErrorCode)
        {
            if (IDErrorList.Count != 0)
            {
                var ErrorList = IDErrorList[ID];
                return (ErrorList.Count != 0) ? " : " + ErrorList[(int)ErrorCode] : "";
            }
            else
            {
                return "";
            }
        }

        private async void WriteToFile(int ID, int Mode, string[] Out)
        {
            string writePath = (Mode == 1) ? IDLogInfoDictionary[ID].TxTFileName : LogName;
            if (writePath == "" || !writePath.Contains(".txt")) writePath = LogName;
            StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default);
            for (int i = 0; i < Out.Count(); i++)
            {
                await sw.WriteLineAsync(Out[i]);
            }
            sw.Close();
        }

        private void WriteToConsole(string[] Out)
        {
            for (int i = 0; i < Out.Count(); i++)
            {
                Console.WriteLine(Out[i]);
            }
        }

        private void GenerateEvent(string[] Out)
        {
            if (LogEventUp != null)
                LogEventUp(this, Out);
        }

        #endregion
    }
}
